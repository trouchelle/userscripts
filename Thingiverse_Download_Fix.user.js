// ==UserScript==
// @name     Thingiverse Download Fix			
// @version  1
// @include  https://www.thingiverse.com/*
// @require  http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require  https://gist.github.com/raw/2625891/waitForKeyElements.js
// @grant    GM_addStyle
// ==/UserScript==

var re = /\d+/;
var array1 = re.exec(document.location);
var id = array1[0];

function replaceShittyAdblockChecks (jNode) {
  // Find A tag among ancestors
  var ancestors = jNode.parents();
	for (var i = 0; i < ancestors.length; i++) {
    if (ancestors[i].tagName == 'A') {
      // Found link, replace it now.
      if (id) {
        var downloadHref = 'https://www.thingiverse.com/thing:' + id + '/zip';
				var link = ancestors[i];
        link.href = downloadHref;
        link.innerHTML = '<div class="button button-primary"><div style="background-image: url(&quot;https://cdn.thingiverse.com/site/assets/download-button.svg&quot;); background-position: center center; background-repeat: no-repeat;" class="i-button left"><span>Download ZIP File</span></div></div>';
        return true;
      }
    }
  }
}

waitForKeyElements (
    "span:contains('Download All')",
    replaceShittyAdblockChecks
);
